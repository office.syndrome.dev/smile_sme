import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    yearlyMember: {}
  },
  mutations: {
    setYearlyMember(state, payload) {
      state.yearlyMember = payload;
      console.log('Yearly Memer:--->', state.yearlyMember);
    }
  },
  actions: {
    async getYearlyMember ({ commit }) {
      await axios.get('https://wegivmerchantapp.firebaseapp.com/exam/bi-member-day-2020-04-01.json')
        .then((response) => {
          commit('setYearlyMember', response.data.data);
        }, (err) => {
          console.log(err);
        });
    }
  },
  modules: {
  }
})
